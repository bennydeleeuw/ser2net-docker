FROM arm32v7/debian

LABEL com.centurylinklabs.watchtower.enable="true"

RUN apt-get update
RUN apt-get install ser2net -y

COPY ser2net.conf /etc/ser2net.conf

EXPOSE 2001

CMD "ser2net" "-d"
